package fr.darneix_bidaud.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private ArrayList<City> cities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        CameraUpdate center=CameraUpdateFactory.newLatLng(new LatLng(46.72, 2.52));
        CameraUpdate zoom=CameraUpdateFactory.zoomTo(5);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
        cities = MainActivity.cities;
        List<String> colors = new ArrayList<>();
        colors.add("#FF5555");
        colors.add("#55FF55");
        colors.add("#5555FF");
        colors.add("#5F5F5F");
        colors.add("#F5F5F5");
        colors.add("#123456");
        colors.add("#654321");
        colors.add("#4E4D9A");
        colors.add("#789456");
        colors.add("#456ABC");
        for(City c : cities){
            LatLng marker = new LatLng(c.getCoord().getLat(), c.getCoord().getLon());
            int random = (int )(Math.random() * 9 + 1);
            //Drawable circleDrawable = getResources().getDrawable(R.drawable.ic_ah);
            //BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);
            mMap.addMarker(new MarkerOptions().position(marker).title(c.getName()).flat(true).icon(getMarkerIcon(colors.get(random))));
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        City city=null;
        for(City c : cities){
            if(c.getName().equals(marker.getTitle())){
                city = c;
            }
        }
        Intent intent = new Intent(MapsActivity.this, DetailCityActivity.class);
        intent.putExtra("city",city);
        startActivity(intent);
        return true;
    }

    public BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
        }
        }
