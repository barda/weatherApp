package fr.darneix_bidaud.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by windaub on 3/18/2017.
 */

public class TabAccueil extends android.support.v4.app.Fragment {

    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tabaccueil, container, false);

        // Création de l'Adapter avec pour paramètre la liste des Users (passage par référence !!!) et un Listener pour gérer le clic
        RecyclerView.Adapter adapter = new CityAdapter(MainActivity.cities, new CityAdapter.OnCityListener() {
            @Override
            public void onCityClick(City city) {
                Intent intent = new Intent(getActivity(), DetailCityActivity.class);
                intent.putExtra("city", city);
                startActivity(intent);
            }

            @Override
            public void onCityLongClick(City city) {
                // Autre action lors du clic long sur un item de la liste
            }
        });

        // Récupération du recyclerView
        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);

        // Affectation du LayoutManager
        recyclerView.setLayoutManager(new LinearLayoutManager(super.getActivity(), LinearLayoutManager.VERTICAL, false));

        // Affectation de l'Adapter
        recyclerView.setAdapter(adapter);
        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        return v;
    }
}

