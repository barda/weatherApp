package fr.darneix_bidaud.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Arrays;

public class SearchActivity extends AppCompatActivity {
    private AutoCompleteTextView text;
    private Button buttonSearch;
    private TextView error;
    private String[] nomVilles = new String[MainActivity.cities.size()];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        text = (AutoCompleteTextView) findViewById(R.id.villeSearch);
        for(int i=0;i<MainActivity.cities.size();i++){
            nomVilles[i]=MainActivity.cities.get(i).getName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, nomVilles);
        text.setAdapter(adapter);
        text.setThreshold(1);
        buttonSearch = (Button) findViewById(R.id.buttonSearch);
        error = (TextView) findViewById(R.id.error);

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean res=false;
                City city=null;
                Log.i("search", "text : "+text.getText().toString());
                for(City c : MainActivity.cities){
                    if(c.getName().equals(text.getText().toString())){
                        res=true;
                        city=c;
                    }
                }
                if(res){
                    Intent intent = new Intent(SearchActivity.this, DetailCityActivity.class);
                    intent.putExtra("city", city);
                    startActivity(intent);
                }else{
                    error.setText("Nous ne connaissons pas cette ville");
                }
            }
        });
    }

}
