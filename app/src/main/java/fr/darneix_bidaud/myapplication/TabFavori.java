package fr.darneix_bidaud.myapplication;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by windaub on 3/17/2017.
 */

public class TabFavori extends android.support.v4.app.Fragment {

    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(MainActivity.FAVORIS.size()==0){
            return inflater.inflate(R.layout.voidlayout, container, false);
        }else{
            View v = inflater.inflate(R.layout.tabfavori, container, false);
            RecyclerView.Adapter adapter = new CityAdapter(MainActivity.FAVORIS, new CityAdapter.OnCityListener() {
                @Override
                public void onCityClick(City city) {
                    Intent intent = new Intent(getActivity(), DetailCityActivity.class);
                    intent.putExtra("city", city);
                    startActivity(intent);
                }

                @Override
                public void onCityLongClick(City city) {
                    // Autre action lors du clic long sur un item de la liste
                }
            });

            // Récupération du recyclerView
            RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);

            // Affectation du LayoutManager
            recyclerView.setLayoutManager(new LinearLayoutManager(super.getActivity(), LinearLayoutManager.VERTICAL, false));

            // Affectation de l'Adapter
            recyclerView.setAdapter(adapter);
            //Returning the layout file after inflating
            //Change R.layout.tab1 in you classes
            return v;
        }
    }
}
