package fr.darneix_bidaud.myapplication;

import android.os.AsyncTask;

/**
 * Created by windaub on 3/15/2017.
 */

public class HugeWork extends AsyncTask<Integer, Integer, Integer> {

    // Method executed before the async task start. All things needed to be
    // setup before the async task must be done here. In this example we
    // simply display a message.
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    // Here is where all the hard work is done. We simulate it by executing
    // a sleep for 1 second, 10 times. Each time the sleep is performed, we update
    // our progress in the method publishProgress(...). This method executes the
    // overridden method onProgressUpdate(...) which updates the progress.
    @Override
    protected Integer doInBackground(Integer... params) {

        // get the initial parameters. For us, this is the initial bar progress = 0
        int progress = ((Integer[])params)[0];

        do {

            // only keep going in case the task was not cancelled
            if (!this.isCancelled()) {
                // execute hard work - sleep
                MainActivity.executeHardWork();
            }
            else {
                // in case the task was cancelled, break the loop
                // and finish this task
                break;
            }

            // upgrade progress
            progress++;
            publishProgress(progress);
        } while (progress <= MainActivity.MAX_PROGRESS);

        return progress;
    }

    // Every time the progress is informed, we update the progress bar
    @Override
    protected void onProgressUpdate(Integer... values) {
        int progress = ((Integer[])values)[0];
        MainActivity.mProgressBar.setProgress(progress);
        super.onProgressUpdate(values);
    }

    // If the cancellation occurs, set the message informing so
    @Override
    protected void onCancelled(Integer result) {
        super.onCancelled(result);
    }

    // Method executed after the task is finished. If the task is cancelled this method is not
    // called. Here we display a finishing message and arrange the buttons.
    @Override
    protected void onPostExecute(Integer result) {
        super.onPostExecute(result);
    }
}
