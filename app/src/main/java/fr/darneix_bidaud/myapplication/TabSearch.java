package fr.darneix_bidaud.myapplication;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by windaub on 3/17/2017.
 */

public class TabSearch extends android.support.v4.app.Fragment {
    private AutoCompleteTextView text;
    private Button buttonSearch;
    private TextView error;
    private String[] nomVilles = new String[MainActivity.cities.size()];

    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tabsearch, container, false);
        text = (AutoCompleteTextView) v.findViewById(R.id.villeSearch);
        for(int i=0;i<MainActivity.cities.size();i++){
            nomVilles[i]=MainActivity.cities.get(i).getName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, nomVilles);
        text.setAdapter(adapter);
        text.setThreshold(1);
        buttonSearch = (Button) v.findViewById(R.id.buttonSearch);
        error = (TextView) v.findViewById(R.id.error);

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean res=false;
                City city=null;
                Log.i("search", "text : "+text.getText().toString());
                for(City c : MainActivity.cities){
                    if(c.getName().equals(text.getText().toString())){
                        res=true;
                        city=c;
                    }
                }
                if(res){
                    Intent intent = new Intent(getActivity(), DetailCityActivity.class);
                    intent.putExtra("city", city);
                    startActivity(intent);
                }else{
                    error.setText("Nous ne connaissons pas cette ville");
                }
            }
        });
        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        return v;
    }
}
