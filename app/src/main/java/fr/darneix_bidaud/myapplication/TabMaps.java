package fr.darneix_bidaud.myapplication;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by windaub on 3/17/2017.
 */

public class TabMaps extends android.support.v4.app.Fragment implements OnMapReadyCallback {
    private MapView mapView;
    private GoogleMap mMap;
    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tabmaps, container, false);
        SupportMapFragment map = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
        map.getMapAsync(this);

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        CameraUpdate center= CameraUpdateFactory.newLatLng(new LatLng(46.72, 2.52));
        CameraUpdate zoom=CameraUpdateFactory.zoomTo(5);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
        List<String> colors = new ArrayList<>();
        colors.add("#FF5555");
        colors.add("#55FF55");
        colors.add("#5555FF");
        colors.add("#5F5F5F");
        colors.add("#F5F5F5");
        colors.add("#123456");
        colors.add("#654321");
        colors.add("#4E4D9A");
        colors.add("#789456");
        colors.add("#456ABC");
        for(City c : MainActivity.cities){
            LatLng marker = new LatLng(c.getCoord().getLat(), c.getCoord().getLon());
            int random = (int )(Math.random() * 9 + 1);
            //Drawable circleDrawable = getResources().getDrawable(R.drawable.ic_ah);
            //BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);
            mMap.addMarker(new MarkerOptions().position(marker).title(c.getName()).flat(true).icon(getMarkerIcon(colors.get(random))));
        }
    }

    public BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }
}