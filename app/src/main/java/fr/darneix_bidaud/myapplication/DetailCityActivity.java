package fr.darneix_bidaud.myapplication;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DetailCityActivity extends FragmentActivity implements OnMapReadyCallback{
    private City city;
    private MapView mapView;
    private GoogleMap mMap;
    private ImageButton bouton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_city);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();

        TextView nomView = (TextView) this.findViewById(R.id.nomVille);
        city = intent.getParcelableExtra("city");
        nomView.setText("Météo : "+city.getName() + " (" + city.getCountry() + ")");

        TextView lat = (TextView) this.findViewById(R.id.lat);
        lat.setText("Latitude : " + city.getCoord().getLat());
        TextView lon = (TextView) this.findViewById(R.id.lon);
        lon.setText("Longitude : " + city.getCoord().getLon());
        String url="http://api.openweathermap.org/data/2.5/weather?id="+city.get_id()+"&appid="+MainActivity.API_KEY+"\n";
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET, // méthode : GET, POST, PUT, DELETE, etc
                url,
                new Response.Listener<String>() { // Listener appelé quand la requête renvoie 200
                    @Override
                    public void onResponse(String response) {
                        //Récupération des informations
                        Gson gson = new Gson();
                        City cityNow = gson.fromJson(response, City.class);
                        cityNow.setName(city.getName());
                        cityNow.setCountry(city.getCountry());
                        cityNow.set_id(city.get_id());
                        //Affichage des infos
                        TextView temp_min = (TextView) findViewById(R.id.temp_min);
                        temp_min.setText(""+(((cityNow.getMain().getTemp_max()-273.15)*100)/100)+" °C");
                        TextView temp = (TextView) findViewById(R.id.temp);
                        temp.setText(""+(((cityNow.getMain().getTemp_max()-273.15)*100)/100)+" °C");
                        TextView temp_max = (TextView) findViewById(R.id.temp_max);
                        temp_max.setText(""+(((cityNow.getMain().getTemp_max()-273.15)*100)/100)+" °C");
                        TextView humidity = (TextView) findViewById(R.id.humidity);
                        humidity.setText("Humidité : "+cityNow.getMain().getHumidity()+" %");
                        TextView pression = (TextView) findViewById(R.id.pression);
                        pression.setText("Pression : "+cityNow.getMain().getPressure()+" hPa");
                        TextView wind_speed = (TextView) findViewById(R.id.wind_speed);
                        wind_speed.setText("Vitesse du vent : "+(cityNow.getWind().getSpeed()*3.6)+" km/h");
                        TextView wind_degree = (TextView) findViewById(R.id.wind_degree);
                        wind_degree.setText("Degrée du vent : "+cityNow.getWind().getDeg()+" °");
                        ImageView image = (ImageView) findViewById(R.id.imageTemps);
                        if((cityNow.getClouds().getAll()>=0)&&(cityNow.getClouds().getAll()<=30)){
                            image.setImageResource(R.drawable.ic_wb_sunny_black_24dp);
                        }else if((cityNow.getClouds().getAll()>=31)&&(cityNow.getClouds().getAll()<=60)){
                            image.setImageResource(R.drawable.ic_cloud_black_24dp);
                        }else{
                            image.setImageResource(R.drawable.ic_grain_black_24dp);
                        }
                        TextView cloud = (TextView) findViewById(R.id.cloud);
                        cloud.setText("Nuage : ");

                        TextView dt = (TextView) findViewById(R.id.dt);
                        long millis = cityNow.getDt()*1000;
                        Date date = new Date(millis);
                        Date today = new Date();
                        int hourDiff=0;
                        if(today.getHours()>=date.getHours()){
                            hourDiff = today.getHours()-date.getHours();
                        }else{
                            hourDiff = date.getHours()-today.getHours();
                        }

                        int minuteDiff=0;
                        if(today.getMinutes()>=date.getMinutes()){
                            minuteDiff = today.getMinutes()-date.getMinutes();
                        }else{
                            minuteDiff = date.getMinutes()-today.getMinutes();
                        }

                        if(hourDiff==0){
                            dt.setText("Dernière mise à jour : "+minuteDiff+" min plus tôt");
                        }else{
                            dt.setText("Dernière mis à jour : "+hourDiff+" heures "+minuteDiff+" min plus tôt");
                        }


                        TextView visibility = (TextView) findViewById(R.id.visibility);
                        visibility.setText("Visibilité : "+cityNow.getVisibility()+" m");
                        TextView sunrise = (TextView) findViewById(R.id.sunrise);
                        millis = cityNow.getSys().getSunrise()*1000;
                        date = new Date(millis);
                        sunrise.setText("Couché du soleil : "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds());
                        TextView sunset = (TextView) findViewById(R.id.sunset);
                        millis = cityNow.getSys().getSunset()*1000;
                        date = new Date(millis);
                        sunset.setText("Levé du soleil : "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds());

                    }
                },
                new Response.ErrorListener() { // Listener appelé en cas d’erreur
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERROR", error.toString());
                    }
                });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringRequest);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabAdd);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean cityExistsInFavoris=false;
                for(City c : MainActivity.FAVORIS){
                    if(city.getName().equals(c.getName())){
                        cityExistsInFavoris=true;
                    }
                }
                if(!cityExistsInFavoris){
                    MainActivity.FAVORIS.add(city);
                }

            }
        });

        FloatingActionButton fabSuppr = (FloatingActionButton) findViewById(R.id.fabSuppr);
        fabSuppr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.FAVORIS.size()!=0){
                    int index=0;
                    for(int i=0;i<MainActivity.FAVORIS.size();i++){
                        if(city.getName().equals(MainActivity.FAVORIS.get(i).getName())){
                            index = i;
                        }
                    }
                    MainActivity.FAVORIS.remove(index);
                }
            }
        });

        bouton = (ImageButton) findViewById(R.id.retour);
        bouton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailCityActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        CameraUpdate center=CameraUpdateFactory.newLatLng(new LatLng(city.getCoord().getLat(), city.getCoord().getLon()));
        CameraUpdate zoom=CameraUpdateFactory.zoomTo(7);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
        LatLng marker = new LatLng(city.getCoord().getLat(), city.getCoord().getLon());
        mMap.addMarker(new MarkerOptions().position(marker).title(city.getName()));
    }
}
