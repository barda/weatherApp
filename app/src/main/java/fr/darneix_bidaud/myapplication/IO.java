package fr.darneix_bidaud.myapplication;

import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by windaub on 3/14/2017.
 */

public class IO {
    public static String readFileFromAssets(String filename,AssetManager assetManager){
        StringBuilder str = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(assetManager.open(filename), "UTF-8"));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                str.append(mLine + "\n");
            }
        } catch (IOException e) {
            Log.e("IO",e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e("IO",e.getMessage());
                }
            }
        }
        return str.toString();
    }

}
