package fr.darneix_bidaud.myapplication;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by windaub on 3/14/2017.
 */

public class CityAdapter extends RecyclerView.Adapter<CityHolder>{

    // Interface permettant de gÃ©rer les actions sur une cellule
    public interface OnCityListener {
        void onCityClick(City user);
        void onCityLongClick(City user);
    }

    private static final String TAG = "CityAdapter";
    private ArrayList<City> items;
    private OnCityListener listener;

    public CityAdapter(ArrayList<City> items, OnCityListener listener) {
        this.items = items;
        this.listener = listener;
    }

    @Override
    public CityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // CrÃ©ation de la cellule
        View view = LayoutInflater.from(parent.getRootView().getContext()).inflate(R.layout.content_main, parent, false);

        // CrÃ©ation du holder Ã  partir de la cellule
        return new CityHolder(view);
    }

    @Override
    public void onBindViewHolder(CityHolder holder, int position) {
        // Bind de la cellule Ã  partir du User Ã  la position
        City city = items.get(position);
        holder.bind(city, listener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}

