package fr.darneix_bidaud.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;

public class FavorisActivity extends AppCompatActivity {
    private TextView header;
    private CityAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favoris);

        // Création de l'Adapter avec pour paramètre la liste des Users (passage par référence !!!) et un Listener pour gérer le clic
        adapter = new CityAdapter(MainActivity.FAVORIS, new CityAdapter.OnCityListener() {
            @Override
            public void onCityClick(City city) {
                Intent intent = new Intent(FavorisActivity.this, DetailCityActivity.class);
                intent.putExtra("city",city);
                startActivity(intent);
            }

            @Override
            public void onCityLongClick(City city) {
                // Autre action lors du clic long sur un item de la liste
            }
        });

        // Récupération du recyclerView
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // Affectation du LayoutManager
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        // Affectation de l'Adapter
        recyclerView.setAdapter(adapter);
    }
}
