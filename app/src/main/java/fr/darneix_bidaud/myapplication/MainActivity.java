package fr.darneix_bidaud.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {
    public static final String API_KEY = "fa3681e7365f0466f5fb3a38ccdbaefb";
    public static final ArrayList<City> FAVORIS = new ArrayList<>();
    public String contentCity;
    public static ArrayList<City> cities;
    private CityAdapter adapter;
    protected static ProgressBar mProgressBar =  null;
    private HugeWork task = null;
    protected static final int MAX_PROGRESS = 10;
    private Gson gson;
    //This is our tablayout
    private TabLayout tabLayout;

    //This is our viewPager
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String contentCity = IO.readFileFromAssets("city_france.json", getAssets());
        Gson gson = new Gson();
        MainActivity.cities = new ArrayList<City>();
        List<String> nomVille = new ArrayList<>();
        for(City  c : gson.fromJson(contentCity,City[].class)){
            if(!nomVille.contains(c.getName())){
                MainActivity.cities.add(c);
                nomVille.add(c.getName());
            }
        }
        MainActivity.cities.remove(0);
        Collections.sort(MainActivity.cities, new Comparator<City>() {
            @Override
            public int compare(City o1, City o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        //Toolbar
        //Adding toolbar to the activity
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setText("Accueil"));
        tabLayout.addTab(tabLayout.newTab().setText("Favori"));
        tabLayout.addTab(tabLayout.newTab().setText("Maps"));
        tabLayout.addTab(tabLayout.newTab().setText("Search"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.pager);

        //Creating our pager adapter
        Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount());

        //Adding adapter to pager
        viewPager.setAdapter(adapter);

        //Adding onTabSelectedListener to swipe views
        tabLayout.setOnTabSelectedListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private void start() {
        // instantiate a new async task
        task = new HugeWork();
        // start async task setting the progress to zero
        task.execute(0);
        // reset progress
        mProgressBar.setProgress(0);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    protected static void executeHardWork() {
    }
}
