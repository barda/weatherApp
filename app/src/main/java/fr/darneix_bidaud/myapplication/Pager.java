package fr.darneix_bidaud.myapplication;

import android.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by windaub on 3/17/2017.
 */

public class Pager extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public Pager(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
    }



    //Overriding method getItem
    @Override
    public android.support.v4.app.Fragment getItem(int position){
        switch(position){
            case 0:
                TabAccueil tabAccueil = new TabAccueil();
                return tabAccueil;
            case 1:
                TabFavori tabFavori = new TabFavori();
                return tabFavori;
            case 2:
                TabMaps tabMaps = new TabMaps();
                return tabMaps;
            case 3:
                TabSearch tabSearch = new TabSearch();
                return tabSearch;
            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}
