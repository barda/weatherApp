package fr.darneix_bidaud.myapplication;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by windaub on 3/14/2017.
 */

public class CityHolder extends RecyclerView.ViewHolder{

    private View view;
    private City city;
    private TextView nomView;

    // Constructeur
    public CityHolder(View itemView) {
        super(itemView);
        this.view = itemView;
        nomView = (TextView) view.findViewById(R.id.nomVille);
    }

    // MÃ©thode qui lie les donnÃ©es du User aux champs correspondant ainsi que le listener
    public void bind(final City city, final CityAdapter.OnCityListener listener) {
        this.city = city;
        nomView.setText(city.getName());
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCityClick(city);
            }
        });
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                listener.onCityLongClick(city);
                return false;
            }
        });
    }
}

