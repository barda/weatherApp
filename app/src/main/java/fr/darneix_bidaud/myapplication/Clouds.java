package fr.darneix_bidaud.myapplication;

import com.google.gson.annotations.SerializedName;

/**
 * Created by windaub on 3/14/2017.
 */

class Clouds {
    @SerializedName("all")
    private int all;

    public Clouds(int all) {
        this.all = all;
    }

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}
